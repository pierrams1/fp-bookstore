@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <h4 class="card-title">New Book Form</h4>
    <div class="card">
        <div class="card-body">
            <p class="card-description"></p>
            <form class="forms-sample" method="POST" action="/book">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" id="title" placeholder="Title">
                </div>
                <div class="form-group">
                    <label for="writer">Writer</label>
                    <input type="text" name="writer" class="form-control" id="writer" placeholder="writer">
                </div>
                <div class="form-group">
                    <label for="year">Year</label>
                    <input type="number" name="year" class="form-control" id="year" placeholder="year">
                </div>
                <div class="form-group">
                    <label for="synopsis">synopsis</label>
                    <textarea name="synopsis" id="synopsis" cols="30" rows="10" class="form-control" id="synopsis" placeholder="synopsis"></textarea>
                </div>
                <button type="submit" class="btn btn-primary mr-2"> Submit </button>
                <button class="btn btn-light">Cancel</button>
            </form>
        </div>
    </div>
</div>
@endsection