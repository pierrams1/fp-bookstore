<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
            <div class="nav-profile-image">
                <img src="{{asset('assets/images/faces/face1.jpg')}}" alt="profile" />
                <span class="login-status online"></span>
                <!--change to offline or busy as needed-->
            </div>
            <div class="nav-profile-text d-flex flex-column pr-3">
                <span class="font-weight-medium mb-0">Henry Klein</span>
            </div>
            {{-- <span class="badge badge-danger text-white ml-3 rounded">3</span> --}}
            </a>
        </li>
            <li class="nav-item">
                <a class="nav-link" href="/book">
                    <i class="mdi mdi-home menu-icon"></i>
                    <span class="menu-title">Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/book/create">
                    <i class="mdi mdi-home menu-icon"></i>
                    <span class="menu-title">Create New Book</span>
                </a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link" href="/">
                    <i class="mdi mdi-home menu-icon"></i>
                    <span class="menu-title">Dashboard</span>
                </a>
            </li> --}}
        </ul>
</nav>