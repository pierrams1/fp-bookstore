<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.master');
});

Route::get('/book', [BookController::class, 'index']);

Route::get('/book/create', [BookController::class, 'create']);

Route::post('/book', [BookController::class, 'store']);

Route::post('/book/{book_id}', [BookController::class, 'show']);

Route::post('/book/{book_id}/edit', [BookController::class, 'edit']);

Route::put('/book/{book_id}', [BookController::class, 'update']);

Route::delete('/book/{book_id}', [BookController::class, 'destroy']);

Route::resource('Book', BookController::class);
